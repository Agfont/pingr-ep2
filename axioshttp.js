import axios from 'axios'

const ax = axios.create({
  baseURL: process.env.jsonserverURL
})

export const register = ({ email, password, username }) => 
  ax.post('/register', { email, password, username })
export const login = ({ email, password }) => 
  ax.post('/login', { email, password })
export const getUserInfo = (id, headers) => 
  ax.get(`/600/users/${id}`, { headers })